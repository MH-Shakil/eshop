<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index(){
        $featured_products = Product::query()->where('trending','1')->orderBy('id','DESC')->with('category')->get();
        $featured_category = Category::query()->where('populer','1')->orderBy('id','DESC')->get();
        return view('front.index',compact('featured_products','featured_category'));
    }
    public function category(){
        $categories = Category::query()->where('status','1')->get();
        return view('front.category',compact('categories'));
    }
    public function categoryShow($id){
        $category = $categories = Category::query()->where('id',$id)->first();
        $category_product = Product::query()->where('category_id',$id)->orderBy('id','DESC')->get();
        return view('front.categoryWaisProduct',compact('category','category_product'));
    }
    public function productDetails($product_slug,$category_slug){
        if(Category::query()->where('slug',$category_slug)->exists()){
            if(Product::query()->where('slug',$product_slug)->exists()){
              $product = Product::query()->where('slug',$product_slug)->first();
              return view('front.product.view',compact('product'));
            }
            else{
                return redirect()->back()->with('status','The line was Broken !');
            }
        }else{
            return redirect()->back()->with('status','The Category not found !');
        }

       
    }
    
}
