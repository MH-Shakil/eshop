@extends('layouts.front')

@section('title')

@endsection

@section('content')
    <div class="py-3 mb-4 shadow-sm bg-warning border-top">
        <div class="container">
            <h6 class="mb-0"> Collection / / </h6>
        </div>
    </div>

    <div class="container">
        <form action="{{route('placorder')}}">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <h6>User Details</h6>
                            <hr>

                            <div class="row checkout-form">
                                <div class="col-md-6">
                                    <input name="id" type="hidden" value="{{auth::user()->id}}">
                                    <label for="firstName">First Name</label>
                                    <input type="text" name="fname" class="form-control"
                                           value="{{auth()->user()->name}}">

                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control" placeholder="Enter Email" value="{{auth()->user()->email}}">

                                    <label for="address1">Address 1</label>
                                    <input type="text" name="address1" class="form-control"
                                           placeholder="Enter address1" value="{{auth()->user()->address1}}">

                                    <label for="city">city</label>
                                    <input type="text" name="city" class="form-control" placeholder="Enter City" value="{{auth()->user()->city}}">

                                    <label for="city">Country</label>
                                    <input type="text" name="country" class="form-control" placeholder="Enter Country" value="{{auth()->user()->country}}">

                                </div>

                                <div class="col-md-6">

                                    <label for="lastName">Last Name</label>
                                    <input type="text" name="lname" class="form-control"
                                           placeholder="Enter Last Name" value="{{auth()->user()->lname}}">

                                    <label for="phoneNumber">Phone Number</label>
                                    <input type="number" name="phone" class="form-control"
                                           placeholder="Enter Phone number" value="{{auth()->user()->phone}}">

                                    <label for="Address2">Address 2</label>
                                    <input type="text" name="address2" class="form-control"
                                           placeholder="Enter Address 2" value="{{auth()->user()->address2}}">

                                    <label for="state">State</label>
                                    <input type="text" name="state" class="form-control"
                                           placeholder="Enter State" value="{{auth()->user()->state}}">

                                    <label for="pinCode">Pin Code</label>
                                    <input type="text" name="pincode" class="form-control"
                                           placeholder="Enter Pin Code" value="{{auth()->user()->pincode}}">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <h6>Order Details</h6>
                            <hr>
                            <table class="table table-border table-striped ">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $total_unique=0;
                                    $total = 0;
                                @endphp
                                @foreach($cartItem as $item)
                                    <tr>
                                        <td>{{$item->product->name}}</td>
                                        <td>{{$item->product->selling_price}}</td>
                                        <td>{{$item->product_quantity}}</td>
                                        <td>{{$item->product->selling_price*$item->product_quantity}}</td>
                                        @php
                                            $total_unique = $item->product->selling_price*$item->product_quantity;
                                            $total = $total + intval($item->product->selling_price)*intval($item->product_quantity);
                                        @endphp
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <hr>
                            <div>
                                Total: <h6 class="float-end mr-8">{{$total}}</h6>
                            </div>
                            <button class="btn btn-primary float-end">Place Order</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')

@endsection
