<div class="form-group col-md-6" style="border-right:1px solid #ddd;">
    <input type="hidden" name="id" value="{{isset($product) ? $product->id : ''}}">
    <label for="">Name</label>
    <input name="name" type="text" class="form-control" placeholder="Enter product name" style="border:none"
        value="{{ isset($product) ? $product->name : old('name') }}">
    @error('name')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>

    <select name="category_id" id="" class="form-control" style="width:100%">
        <option value="{{isset($product->category_id) ? $product->category_id : 'Select Category' }} " Seleced>{{isset($product->category_id) ? $product->category->name : 'Select Category' }}</option>
        @foreach($categories as $item)
        <option value="{{ $item->id}}">{{ $item->name}}</option>
        @endforeach
    </select>
    <hr>

    <label for="">Original Price</label>
    <input name="original_price" type="number" class="form-control" placeholder="Enter product original price"
        style="border:none" value="{{ isset($product) ? $product->original_price : old('original_price') }}">
    @error('original_price')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>

    <label for="">Selling Price</label>
    <input name="selling_price" type="number" class="form-control" placeholder="Enter product Sellign price"
        style="border:none" value="{{ isset($product) ? $product->selling_price : old('selling_price') }}">
    @error('selling_price')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>
    <label for="">Tax</label>
    <input name="tax" type="number" class="form-control" placeholder="Enter product tax" style="border:none"
        value="{{ isset($product) ? $product->tax : old('tax') }}">
    @error('tax')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>
    <label for="">Quantity</label>
    <input name="quantity" type="number" class="form-control" placeholder="Enter product quantity" style="border:none"
        value="{{ isset($product) ? $product->quantity : old('quantity') }}">
    @error('quantity')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>


    <label for="">Status</label>
    <input name="status" type="checkbox" @if (isset($product)) {{ $product->status == 1 ? 'checked' : '' }} @endif>
    <label for="">trending</label>
    <input name="trending" type="checkbox" class="" @if (isset($product)) {{ $product->trending ==1 ? 'checked' : '' }}
        @endif>
</div>
<div class="form-group col-md-6">

    <label for="">Description</label>
    <input name="description" type="text" class="form-control" placeholder="Enter product Description"
        style="border:none" value="{{ isset($product) ? $product->description : old('description') }}">
    @error('description')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>

    <label for="">Image : </label>

    @if(isset($product))
        @if (isset($product->image))
        if
            <img name="image" class="update_image formControl" src="{{asset($product->image)}}" alt="{{$product->image}}"
                height="100px" width="100px">
            <input name="new_image" type="file" style="border:none">
        @else
        else
            <input name="new_image" type="file" style="border:none">
        @endif
    @else
    <input name="image" type="file" style="border:none">
    @endif
    @error('image')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>

    <label for="">Small Description</label>
    <input name="small_description" type="text" class="form-control" placeholder="Enter product Samll Description"
        style="border:none" value="{{ isset($product) ? $product->small_description : old('small_description') }}">
    @error('small_description')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>



    <label for="">Meta Description</label>
    <input name="meta_description" type="text" class="form-control" placeholder="Enter product Meta Description"
        style="border:none" value="{{ isset($product) ? $product->meta_description : old('meta_description') }}">
    @error('meta_description')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>
    <label for="">Meta title</label>
    <input name="meta_title" type="text" class="form-control" placeholder="Enter product Meta title" style="border:none"
        value="{{ isset($product) ? $product->meta_title : old('meta_title') }}">
    @error('meta_title')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>

    <label for="">Meta Keyword</label>
    <input name="meta_keyword" type="text" class="form-control" placeholder="Enter product Meta keyword"
        style="border:none" value="{{ isset($product) ? $product->meta_keyword : old('meta_keyword') }}">
    @error('meta_keyword')
    <span class="text-danger">{{ $message }}</span>
    @enderror
    <hr>
</div>
<button type="submit" class="btn btn-primary">{{ $button }}</button>
<!-- <div class="card-body">
    <div class="form-group col-md-12">
        <label for="exampleInputname">Name</label>
        <input style="border:none;" type="text" name="name" class="form-control" id="exampleInputName" placeholder="Enter user Name"
            value="{{ isset($user) ? $user->name : old('name') }}">
        @error('name')
    <span class="text-danger">{{ $message }}</span>
@enderror
    </div>
    <hr>
    <div class="form-group">
        <label for="exampleInputname">Email</label>
        <input type="email" name="email" class="form-control" id="exampleInputName" placeholder="Enter user email"
            value="{{ isset($user) ? $user->email : old('email') }}">
        @error('email')
    <span class="text-danger">{{ $message }}</span>
@enderror
    </div>
    <div class="form-group">
        <label for="exampleInputname">Password</label>
        <input type="password" name="password" class="form-control" id="exampleInputName"
            placeholder="Enter user password" value="{{ isset($user) ? $user->password : old('password') }}">
        @error('password')
    <span class="text-danger">{{ $message }}</span>
@enderror
    </div>
    <div class="form-group">
        <label for="exampleInputname">Image</label>
        <input type="file" name="image" class="form-control" id="exampleInputName" placeholder="Enter user image"
            value="{{ isset($user) ? $user->image : old('image') }}">
        @error('image')
    <span class="text-danger">{{ $message }}</span>
@enderror
    </div>
    <div class="form-group">
        <label for="exampleInputDescription">Description</label>
        <textarea name="description" id="" cols="60" rows="4"
            class="form-control">{{ isset($user) ? $user->description : old('description') }}</textarea>
    </div>
</div>
<div class="card-footer">
    <button type="submit" class="btn btn-primary">{{ $button }}</button>
</div> -->
