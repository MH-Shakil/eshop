<?php

namespace App\Service;

class EshopFlagMessage
{

    const STATUS_TRUE = true;
    CONST STATUS_FALSE = false;
    const ONLY_SUCCESS = 200;
    CONST SUCCESS_WITH_DATA = 201;
    const LOGIN_REQUIRED   = 600;

    CONST LOGIN_REQUIRED_MESSAGE = 'Login to continue.';

    public static function eshop_json_response(
        $code = self::ONLY_SUCCESS,
        $message = null,
        $flag = false,
        $body = null)
    {

        return response()->json([
            'status'=> $message,
            "code" =>$code,
            "message" => $message,
            "data" =>$body
        ]);

    }

}
