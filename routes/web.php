<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CityController;
use App\Http\Controllers\StateController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [FrontendController::class, 'index'])->name('website');



Route::get('/front-category', [FrontendController::class, 'category'])->name('front.category');
Route::get('/front-category-show/{id}', [FrontendController::class, 'categoryShow']);
Route::get('/productDetails/{product?}/{slug?}', [FrontendController::class, 'productDetails']);
Route::Post('/add-to-cart', [CartController::class, 'addToCart']);
Route::get('/front', [CartController::class, 'showCart']);
Route::post('/cartDelete', [CartController::class, 'cartDelete']);
Route::post('/cartUpdate', [CartController::class, 'cartUpdate']);
Route::get('/checkout', [CheckoutController::class, 'index']);
Route::get('/placorder', [CheckoutController::class, 'placorder'])->name('placorder');

// user middleware route
Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('my-order',[OrderController::class,'index'])->name('myOrder');
});

// admin route
 Route::middleware(['auth', 'isAdmin'])->group(function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
     });
    Route::get('/category',[CategoryController::class,'index'])->name('category');
    Route::get('/categoryAdd',[CategoryController::class,'create'])->name('category.add');
    Route::post('/categorystore',[CategoryController::class,'store'])->name('category.store');
    Route::get('/categoryedit/{id}',[CategoryController::class,'edit'])->name('category.edit');
    Route::post('/categoryupdate',[CategoryController::class,'update']);
    Route::delete('/categorydelete/{id}',[CategoryController::class,'destroy'])->name('category.delete');
    Route::resource('/product', ProductController::class);

    //route for relation
    Route::get('/country',[CountryController::class,'index']);
    Route::get('/state',[StateController::class,'index']);
    Route::get('/city',[cityController::class,'index']);
    Route::get('/countryAdd',[CountryController::class,'create']);
    Route::get('/stateAdd',[StateController::class,'create']);
    Route::get('/cityAdd',[CityController::class,'create']);
    Route::post('/countryStore',[CountryController::class,'store'])->name('countryStore');
    Route::post('/stateStore',[StateController::class,'store'])->name('stateStore');
    Route::post('/cityStore',[CityController::class,'store'])->name('cityStore');
 });

Route::get('/clear', function(){
    \Artisan::call('route:clear');
    \Artisan::call('config:cache');
    \Artisan::call('cache:clear');
});


Route::get('/productImage',function(){
    $data = \App\Models\Product::all();
    $id =172;
    foreach($data as $item){
        $item->image = "https://picsum.photos/id/".$id."/640/480.jpg";
        $item->update();
        $id++;
    }
    return $data;
});

Route::get('/catImage',function(){
    $data = \App\Models\Category::all();
    $id =172;
    foreach($data as $item){
        $item->image = "https://picsum.photos/id/".$id."/640/480.jpg";
        $item->update();
        $id++;
    }
    return $data;
});

