@extends('layouts.admin')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Product List</h1>
            </div><!-- /.col -->

            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Home</a></li>
                    <li class="breadcrumb-item active"> <a href="{{ route('product.create') }}">add new category</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <table class="table table-bordered table-hover" >

                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Selling Price</th>
                    <th>Category</th>
                    <th>Quantity</th>
                    <th>Status</th>
                    <th>image</th>
                    <th>Action</th>
                </tr>

                @foreach ($products as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->original_price }}</td>
                    <td>{{ $item->selling_price }}</td>
                    <td> <span class="badge badge-primary" style="padding:5px;">{{ $item->category->name }}</span></td>
                    <td>{{ $item->quantity }}</td>
                    <td>{{ $item->status }}</td>
                    <td>
                        <img src="{{ $item->image }}" class="img-fluid cat-image" alt="{{ $item->image }}">
                    </td>
                    <td class="d-flex">
                        <a href="{{ route('product.edit', [$item->id]) }}" class="btn btn-sm btn-primary mr-1"><i
                                class="fas fa-edit"></i></a>
                        <form action="{{ route('product.destroy', [$item->id]) }}" class="mr-1" method="POST">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-sm btn-danger "><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach

            </table>
        </div>
        <!-- /.card-body -->
    </div>
</div>
<!-- /.card -->
</div>
<div>
    @endsection