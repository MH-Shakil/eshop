@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">

            <a class="btn btn-info" href="{{URL::to('countryAdd')}}">Country Add</a>
            <a class="btn btn-info" href="{{URL::to('stateAdd')}}">State Add</a>
            <a class="btn btn-info" href="{{URL::to('cityAdd')}}">City Add</a>
        </div>
    </div>

@endsection
