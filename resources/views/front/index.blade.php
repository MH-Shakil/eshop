@extends('layouts.front')

@section('title')
Welcome To E-Shop
@endsection

@section('content')
@include('layouts.inc.slider')
<div class="py-5">
    <div class="container">
        <h3>Featur Product's</h3>
        <div class="row bg-secondary">
            <div class="owl-carousel owl-theme">
                @foreach ($featured_products as $product)
                <div class="item">
                    <a href="{{url('productDetails/'.$product->slug.'/'.$product->category->slug)}}">
                        <div class="card mt-4">
                            <img style="height: 220px;" class="shadow bordered" src="{{$product->image}}"
                                alt="{{$product->image}}">
                            <div class="card-body shadow bg-dark" style="border: 1px solid gray">
                                <h5>{{$product->name}}</h5>
                                <small>{{$product->original_price}}</small>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <hr>
    </div>
</div>
<div class="py-5 mt-4">
    <div class="container mt4">
        <h3>Featur Category</h3>
        <div class="row" style="background:#5D8393;">
            <div class="owl-carousel owl-theme">
                @foreach ($featured_category as $category)
                <div class="item">
                    <a href="{{ url('front-category-show', [$category->id]) }}" class="text-white">
                        <div class="card mt-4">
                            <img style="min-height: 220px;" class="shadow bordered" src="{{$category->image}}"
                                alt="{{$category->image}}">
                            <div class="card-body shadow" style="border: 1px solid gray; background:#41A1CA;">
                                <h5>{{$category->name}}</h5>
                                <small>{{$category->description}}</small>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <hr>
    </div>
</div>
@endsection

@section('script')
<script>
$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        }
    }
})
</script>
@endsection