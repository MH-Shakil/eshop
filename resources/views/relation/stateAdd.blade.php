@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <a class="btn btn-info" href="{{ URL::to('countryAdd') }}">Country Add</a>
            <a class="btn btn-info" href="{{ URL::to('stateAdd') }}">State Add</a>
            <a class="btn btn-info" href="{{ URL::to('cityAdd') }}">City Add</a>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3>State</h3>
        </div>
        <div class="card-body" style="background:#ddd;">
            <form action="{{ route('stateStore') }}" method="POST">
                @method('post')
                @csrf
                <input width="20px" name="name" type="text" class="form-control" placeholder="Enter category name"
                    style="border:none"><br>
                <select style="width:200px" name="country_id" id="" class="form-control btn btn-info">
                    <option desable>Select Country</option>
                    @foreach ($country as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
                <input type="submit" class="btn btn-primary">
            </form>
            <hr>
            <table class="table table-border table-hover">
                <thead>
                    <th>Name</th>
                    <th>Country</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->country->name ?? '' }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection
