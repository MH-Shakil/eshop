@extends('layouts.admin')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Category List</h1>
                </div><!-- /.col -->
                
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('category') }}">Home</a></li>
                        <li class="breadcrumb-item active"> <a href="{{ route('category.add') }}">add new category</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table" class="cat-table">

                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>image</th>
                        <th>Action</th>
                    </tr>


                    @foreach ($categories as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->description }}</td>
                            <td>
                                <img src="{{ $item->image }}" class="img-fluid cat-image" alt="{{ $item->image }}">
                            </td>
                            <td class="d-flex">
                                <a href="{{ route('category.edit', [$item->id]) }}" class="btn btn-sm btn-primary mr-1"><i
                                        class="fas fa-edit"></i></a>
                                <form action="{{ route('category.delete', [$item->id]) }}" class="mr-1"
                                    method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-sm btn-danger "><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    <!-- /.card -->
    </div>
    <div>
    @endsection
