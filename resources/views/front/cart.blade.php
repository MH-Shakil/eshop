@extends('layouts.front')

@section('title')
My Cart
@endsection

@section('content')
<div class="py-3 mb-4 shadow-sm bg-warning border-top">
    <div class="container">
        <h6 class="mb-0"> Collection / Cart</h6>
    </div>
</div>
<div class="py-5">
    <div class="container">
        <div class="card shadow">
            <div class="card-body m-1 text-white">
              @php $total = 0; @endphp
                @foreach ($cartItem as $item)
                <div class="row border product_data">
                    <div class="col-md-2 " style="height: 100px">
                        <img src="{{asset($item->product->image)}}" style="margin-left:10% " height="100px" width="100px" alt="{{asset($item->product->image)}}">
                    </div>
                    <div class="col-md-3 " style="height: 100px; padding:3%">
                        <h3>{{$item->product->name}}</h3>
                    </div>
                    <div class="col-md-5 " style="height: 100px">
                        <input type="hidden" name="" class="prod_id" value="{{$item->product_id}}">
                        <label for="" style="margin-left: 34%; border-bottom: 0px solid rgb(43, 61, 43);"> Quantity</label>
                        <div style="display:inline-block; background:#325e46" >
                            <span style="padding: 8px 9px;">Price : {{$item->product->selling_price}}</span>
                            <button class="btn btn-secondary dec-btn" style="display: inline-block; margin-right: -2% !important;">-</button>
                            <input class="form-control text-center quantity " type="text" style="display: inline-block;
                            width: 60px; padding-bottom: 9px;" value="{{$item->product_quantity}}">
                            <button class="btn btn-secondary inc-btn" style="display: inline-block; margin-left: -2% !important;">+</button>
                            <span style="padding: 8px 9px;">Total : {{$item->product_quantity*$item->product->selling_price}}</span>
                        </div>
                    </div>
                    <div class="col-md-2" style=" margin:auto">
                        <button class="btn btn-danger del-btn">Remove</button>
                    </div>
                </div>
                  @php
                    $total += $item->product->selling_price * $item->product_quantity ;
                 @endphp
                @endforeach
            </div>
            <div class="card-footer border">
              <h6 style="display:inline-block;">Total Price : {{ $total }}</h6>
              <a href="{{url('checkout')}}" class="btn btn-outline-success float-end">Checkout</a>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
  $(document).ready(function() {
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    // product increment function
      $('.inc-btn').click(function(e) {
          e.preventDefault(e);
          var inc_value = $(this).closest('.product_data').find('.quantity').val();
          var value = parseInt(inc_value, 10);
          value = isNaN(value) ? 0 : value;
          if (value < 10) {
              value++;
              // $('.quantity').val(value);
              $(this).closest('.product_data').find('.quantity').val(value);
          } else {
              alert('Yout limit is 10')
          }
          product_quantity = $(this).closest('.product_data').find('.quantity').val();
           prod_id = $(this).closest('.product_data').find('.prod_id').val();
       $.ajax({
          method: "post",
          url: "/cartUpdate",
          data: {
              'product_id': prod_id,
              'product_quantity': product_quantity,
          },
          success: function (response){
            window.location.reload();
              swal(response.status,'Successfully Updated');

          }
      });
      });

      // product cart decrement function
       $('.dec-btn').click(function(e) {
        e.preventDefault(e);
        var dec_value = $(this).closest('.product_data').find('.quantity').val();
        var value = parseInt(dec_value, 10);
        value = isNaN(value) ? 0 : value;

        if (value > 1) {
            value--;
            $(this).closest('.product_data').find('.quantity').val(value);
        } else {
            alert('Your needed at list one quantity')
        }
           product_quantity = $(this).closest('.product_data').find('.quantity').val();
           prod_id = $(this).closest('.product_data').find('.prod_id').val();
       $.ajax({
          method: "post",
          url: "/cartUpdate",
          data: {
              'product_id': prod_id,
              'product_quantity': product_quantity,
          },
          success: function (response){
            window.location.reload();
              swal(response.status,'success');

          }
      });
    });

      // cart delete function
     $('.del-btn').click(function(){
        prod_id = $(this).closest('.product_data').find('.prod_id').val();
       $.ajax({
          method: "post",
          url: "/cartDelete",
          data: {
              'product_id': prod_id
          },
          success: function (response){
            window.location.reload();
              swal(response.status,'success');

          }
      });
     })

  });
</script>
@endsection
