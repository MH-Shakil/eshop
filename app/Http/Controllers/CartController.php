<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use App\Service\EshopFlagMessage;
use App\Service\EshopService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
  public function addToCart(Request $request){
        $product_id = $request->input('product_id');
        $product_quantity = $request->input('product_quantity');

        if(EshopService::is_authorized())
        {
            $product_check = Product::query()->where('id',$product_id)->first();
            if($product_check)
            {
                if(Cart::where('product_id',$product_id)->where('user_id',Auth::id())->exists())
                {
                    return response()->json(['status' => $product_check->name." Already Added to Cart"]);
                }
                else
                {
                    $cartItem = new Cart();
                    $cartItem->product_id = $product_id;
                    $cartItem->user_id = Auth::id();
                    $cartItem->product_quantity = $product_quantity;
                    $cartItem->save();
                    return response()->json(['status' => $product_check->name." Added to Cart"]);

                }
            }
        }
        else
        {
            return EshopFlagMessage::eshop_json_response(
                EshopFlagMessage::LOGIN_REQUIRED,
                EshopFlagMessage::LOGIN_REQUIRED_MESSAGE,
                EshopFlagMessage::STATUS_FALSE
            );
        }
    }




    public function showCart(){
        if(Auth::check()){
            $cartItem = Cart::query()->where('user_id',Auth::id())
                                     ->with('product')
                                     ->with('user')
                                     ->get();
            return view('front.cart',compact('cartItem'));
        }else{
            return redirect()->back()->with('status','Login for First For add to cart');
        }
    }
    public function cartDelete(Request $request)
    {

      if (Auth::check())
      {
        $product_id = $request->input('product_id');
        if(Cart::where('product_id',$product_id)->where('user_id',Auth::id())->exists())
        {
          $cartItem = Cart::where('product_id',$product_id)->where('user_id',Auth::id())->first();
          $cartItem->delete();
          return response()->json(['status'=>'Cart Item Delete Successfully']);
        }
        else
        {
           return response()->json(['status'=>'Cart Item not found']);
        }

      }
      else
      {
         return EshopFlagMessage::eshop_json_response(
           EshopFlagMessage::LOGIN_REQUIRED,
           EshopFlagMessage::
         );
      }

    }
    public function cartUpdate(Request $request)
    {
      if (Auth::check())
      {
        $product_id = $request->input('product_id');
        $product_quantity = $request->input('product_quantity');
        if(Cart::where('product_id',$product_id)->where('user_id',Auth::id())->exists())
        {
          $cartItem = Cart::where('product_id',$product_id)->where('user_id',Auth::id())->first();
          $cartItem->product_quantity = $product_quantity;
          $cartItem->Update();
          return response()->json(['status'=>'Cart Item Update Successfully']);
        }
        else
        {
           return response()->json(['status'=>'Cart Item not found']);
        }

      }
      else
      {
         return response()->json(['status'=>'login first']);
      }

    }
}
