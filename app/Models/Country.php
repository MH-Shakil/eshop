<?php

namespace App\Models;

use App\Models\State;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Country extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    public function states(){
        $this->hasMany(State::class);
    }
    public function cityState(Type $var = null)
    {
        return $this->hasOneThrough(State::class, City::class);
    }
}
