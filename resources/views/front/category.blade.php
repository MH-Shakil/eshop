@extends('layouts.front')

@section('title')
Welcome To E-Shop
@endsection

@section('content')

<div class="py-5">
    <div class="container">
        <h3>All Categories</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    @foreach ($categories as $item)
                    <div class="col-md-4">
                        <a href="{{ url('front-category-show', [$item->id]) }}" class="text-white">
                            <div class="card mt-4">
                                <img style="height: 300px;" class=" " src="{{$item->image}}" alt="{{$item->image}}">
                                <div class="card-body bg-light" style="">
                                    <h5>{{$item->name}}</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        }
    }
})
</script>
@endsection