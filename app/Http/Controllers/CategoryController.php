<?php

namespace App\Http\Controllers;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Session;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::query()->orderBy('id','DESC')->get();
        return view('admin.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->slug = $request->name;
        $category->description = $request->description;
        $category->status = $request->status == TRUE ? '1' : '0';
        $category->populer = $request->populer == TRUE ? '1' : '0';
        $category->meta_title = $request->meta_title;
        $category->meta_description = $request->meta_description;
        $category->meta_keyword = $request->meta_keyword;
        if($request->file('image')){
            $file= $request->file('image');
            $filename='public/category/'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file-> move(public_path('public/category'), $filename);
            $category['image']= $filename;
        }
        $category->save();
        Session::flash('status','Category Added Successfully');
        return redirect()->route('category.add');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($item)
    {
        $category = Category::query()->Where('id',$item)->first();
        return view('admin.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->slug = $request->name;
        $category->description = $request->description;
        $category->status = $request->status == TRUE ? '1' : '0';
        $category->populer = $request->populer == TRUE ? '1' : '0';
        $category->meta_title = $request->meta_title;
        $category->meta_description = $request->meta_description;
        $category->meta_keyword = $request->meta_keyword;
        if($request->file('image')){
            $file= $request->file('image');
            $filename='public/category/'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file-> move(public_path('public/category'), $filename);
            $category->image= $filename;
        } else{
            $category['image'] = $category->image;
        }
        $category->update();
        Session::flash('status','Category Successfully Updated');
        return redirect()->route('category')
        ->with('warning',"Don't Open this link");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $id)
    {
        if ($id) {
            File::delete($id->image);
            $id->delete();
            Session::flash('status','Data Successfully Deleted');
            return redirect()->back();
    
          }
    }
}
