<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected  $fillable = [
      'fname',
      'lname',
      'email',
      'phone',
      'address1',
      'address2',
      'phone',
      'city',
      'state',
      'country',
      'pincode',
      'status',
      'message',
      'tracking_no',
    ];
}
