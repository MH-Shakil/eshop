<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use File;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::query()->orderBy('id','DESC')->with('category')->get();
        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::query()->get();
        return view('admin.product.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->slug = $request->name;
        $product->category_id = $request->category_id;
        $product->description = $request->description;
        $product->small_description = $request->small_description;
        $product->original_price = $request->original_price;
        $product->selling_price = $request->selling_price;
        $product->quantity = $request->quantity;
        $product->tax = $request->tax;
        $product->status = $request->status == TRUE ? '1' : '0';
        $product->trending = $request->trending == TRUE ? '1' : '0';
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;
        $product->meta_keyword = $request->meta_keyword;
        if($request->file('image')){
            $file= $request->file('image');
            $filename='public/product/'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file-> move(public_path('public/product'), $filename);
            $product['image']= $filename;
        }

        $product->save();
        Session::flash('status','Product Added Successfully');
        return redirect()->route('product.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::query()->get();
       return view('admin.product.edit',compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->name = $request->name;
        $product->slug = $request->name;
        $product->category_id = $request->category_id;
        $product->description = $request->description;
        $product->small_description = $request->small_description;
        $product->original_price = $request->original_price;
        $product->selling_price = $request->selling_price;
        $product->quantity = $request->quantity;
        $product->tax = $request->tax;
        $product->status = $request->status == TRUE ? '1' : '0';
        $product->trending = $request->trending == TRUE ? '1' : '0';
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;
        $product->meta_keyword = $request->meta_keyword;
        if($request->file('new_image')){
            $file= $request->file('new_image');
            $filename='public/product/'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file-> move(public_path('public/product'), $filename);
            $product['image']= $filename;
        }else{
            $product['image']= $product->image;
        }

        $product->update();
        Session::flash('status','Product Update Successfully');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if ($product) {
            File::delete($product->image);
            $product->delete();
            Session::flash('status','Data Successfully Deleted');
            return redirect()->back();
          }
    }
}