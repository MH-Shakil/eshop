@extends('layouts.front')

@section('title')
Welcome To E-Shop
@endsection
@section('content')
<div class="py-3 mb-4 shadow-sm bg-warning border-top">
    <div class="container">
        <h6 class="mb-0"> Collection / {{ $category->name }} </h6>
    </div>
</div>
<div class="py-5">
    <div class="container">
        <h3>All {{ $category->name }} Products</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    @foreach ($category_product as $item)
                    <div class="col-md-4">
                    <a href="{{url('productDetails/'.$item->slug.'/'.$item->category->slug)}}">
                            <div class="card mt-4">
                                <img style="height: 300px;" class=" " src="{{asset($item->image)}}"
                                    alt="{{asset($item->image)}}">
                                <div class="card-body bg-light" style="">
                                    <h5>{{$item->name}}</h5>
                                    <small>{{$item->description}}</small><br>
                                    <span class="float-start">{{$item->selling_price}}</span>
                                    <span class="float-end"><s>{{$item->original_price}}</s></span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        }
    }
})
</script>
@endsection