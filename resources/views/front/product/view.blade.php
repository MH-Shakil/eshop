@extends('layouts.front')

@section('title')
{{ $product->name }}
@endsection

@section('content')
<div class="py-3 mb-4 shadow-sm bg-warning border-top">
    <div class="container">
        <h6 class="mb-0"> Collection / {{ $product->category->name }} / {{ $product->name }} </h6>
    </div>
</div>

<div class="container">
    <div class="card shadow">
        <div class="card-body product_data">
            <div class="row">
                <div class="col-md-4 border-right">
                    <img src="{{asset($product->image)}}" class="w-100" alt="{{$product->image}}">
                </div>
                <div class="col-md-8">
                    <h2 class="mb-0">
                        {{ $product->name }}
                        <label style="font-size: 16px;" for="trending"
                            class="float-end badge bg-danger trending-tag">{{ $product->trending == 1 ? 'Trending' : '' }}</label>
                    </h2>
                    <hr>
                    <label for="" class="me-3 badge bg-warning p-2">Original Price : ৳
                        <s>{{ number_format($product->original_price, 2)}}</s></label>
                    <label for="" class="fw-bold badge bg-info p-2">Original Price : ৳
                        {{ number_format($product->selling_price, 2)}}</label>
                    <p class="mt-3">
                        {!! $product->small_description !!}
                    </p>
                    <hr>
                    @if($product->quantity>0)
                    <label for="" class="bg-success badge">In Stock</label>
                    @else
                    <label for="" class="bg-danger">Out Of Stock</label>
                    @endif
                    <div class="row mt-2">
                        <label for="Quantity">Quantity</label>
                        <div class="input-group text-center mt-4 col-md-2">
                            <input type="hidden" value="{{$product->id}}" class="product_id">
                            <button class="input-group-text decrement-btn" style="height: 38px;">-</button>
                            <input type="text" name="quantity" value="1" id="quantity"
                                class="form-control text-center quantity quantity-input">
                            <button class="input-group-text increment-btn" style="height: 38px;">+</button>
                        </div>
                        <div class="col-md-10">
                            <br />
                            <button type="button" class="btn btn-success  me-3 float-start">Add to Wishlist <i
                                    class="fa fa-heart"></i></button>
                            <button type="button" class="btn btn-primary addToCart me-3 float-start">Add to Cart <i
                                    class="fa fa-shopping-cart"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
    $('.addToCart').click(function(e) {
        // alert('hello');
        e.preventDefault(e);
        var product_id = $(this).closest('.product_data').find('.product_id').val();
        var product_quantity = $(this).closest('.product_data').find('.quantity').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: "POST",
            url: "/add-to-cart",
            data: {
                'product_id': product_id,
                'product_quantity': product_quantity,
            },
            success: function (response){
                swal(response.status);
            }
        });

    });
    $('.increment-btn').click(function(e) {
        e.preventDefault(e);
        var inc_value = $('#quantity').val();
        var value = parseInt(inc_value, 10);
        value = isNaN(value) ? 0 : value;
        if (value < 10) {
            value++;
            $('.quantity-input').val(value);
        } else {
            alert('Yout limit is 10')
        }
    });
    $('.decrement-btn').click(function(e) {
        e.preventDefault(e);
        // alert('hello world')
        var dec_value = $('#quantity').val();
        var value = parseInt(dec_value, 10);
        value = isNaN(value) ? 0 : value;

        if (value > 1) {
            value--;
            $('#quantity').val(value);
        } else {
            alert('Your needed at list one quantity')
        }
    });


});
</script>
@endsection
