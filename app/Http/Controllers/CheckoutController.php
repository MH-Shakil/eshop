<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use Illuminate\Bus\UpdatedBatchJobCounts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
   public function index(){
    $cartItem = Cart::where('user_id',Auth::id())->get();
    return view('front.product.checkout',compact('cartItem'));
   }
   public function placorder(Request $request){

       $order =  new Order();

       $order->fname= $request-> fname;
       $order->email = $request->email;
       $order->lname = $request->lname;
       $order->phone = $request->phone;
       $order->address1 = $request->address1;
       $order->address2 = $request->address2;
       $order->city = $request->city;
       $order->state = $request->state;
       $order->country = $request->country;
       $order->pincode = $request->pincode;
       $order->tracking_no = '563456';
       $order->save();
       $carts = Cart::query()->get();
       $orderItem = OrderItem::query()->get();
       foreach ($carts as $item) {
           $orderItem =  new OrderItem();
//           dd($orderItem);
           $prices = Product::query()->where('id',$item->product_id)->first()->pluck('selling_price')->first();
           $orderItem->order_id = $request->id;
           $orderItem->product_id = $item->product_id;
           $orderItem->quantity	 = $item->product_quantity;
           $orderItem->price = $prices;
//           dd($orderItem);
           $orderItem->save();
           $item->delete();
       }


       return redirect('/')->with('status','Congratulation! Your Order successfully done');
   }
}
