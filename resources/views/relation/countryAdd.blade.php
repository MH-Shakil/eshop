@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <a class="btn btn-info" href="{{URL::to('countryAdd')}}">Country Add</a>
            <a class="btn btn-info" href="{{URL::to('stateAdd')}}">State Add</a>
            <a class="btn btn-info" href="{{URL::to('cityAdd')}}">City Add</a>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3>Country</h3>
        </div>
        <div class="card-body" style="background:#ddd;">
            <form action="{{route('countryStore')}}" method="POST">
                @method('post')
                @csrf
                <input width="20px" name="name" type="text" class="form-control" placeholder="Enter category name" style="border:none">
                <input type="submit">
            </form>
            <hr>
            <table class="table table-border table-hover">
                <thead>
                    <th>Name</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($data as $data)
                        <tr>
                            <td>{{$data->name}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

@endsection
